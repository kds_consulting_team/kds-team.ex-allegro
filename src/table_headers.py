BILLING_ENTRIES_COLUMNS = ['id', 'occured_at', 'type_id', 'type_name', 'offer_id', 'offer_name', 'value_amount',
                           'value_currency', 'tax_percentage', 'balance_amount', 'balance_currency']
BILLING_ENTRIES_RAW_COLUMNS = ['id', 'occuredAt', 'type_id', 'type_name', 'offer_id', 'offer_name', 'value_amount',
                               'value_currency', 'tax_percentage', 'balance_amount', 'balance_currency']
BILLING_ENTRIES_JSON_COLUMNS = []
BILLING_ENTRIES_PK = ['id']
