import logging
import sys

from keboola.component.dao import OauthCredentials

from client import AllegroClient
from result import Writer
from keboola.component.base import ComponentBase
from keboola.component.exceptions import UserException
import keboola.utils.date as dutils
import table_headers as hdr

# Cfg Parameters
KEY_OBJECTS = 'objects'
KEY_DATE_FROM = 'date_from'
KEY_INCREMENTAL = 'incremental'

# Image parameters
KEY_REDIRECT_URI = 'redirect_uri'

# Required
REQUIRED_PARAMETERS = []
REQUIRED_IMAGE_PARS = [KEY_REDIRECT_URI]


class Component(ComponentBase):

    def __init__(self):
        super().__init__()

        self.validate_configuration_parameters(REQUIRED_PARAMETERS)
        self.validate_image_parameters(REQUIRED_IMAGE_PARS)

        auth = self.configuration.oauth_credentials
        refresh_token = self.determine_refresh_token(auth)

        self.client = AllegroClient(client_key=auth.appKey, client_secret=auth.appSecret, refresh_token=refresh_token,
                                    redirect_uri=self.configuration.image_parameters[KEY_REDIRECT_URI])

    def determine_refresh_token(self, oauth_credentials: OauthCredentials):

        state = self.get_state_file()

        if '#refresh_token' in state:
            return state['#refresh_token']
        else:
            return oauth_credentials.data['refresh_token']

    def get_date(self, date_to_parse: str):

        date = dutils.get_past_date(date_to_parse)
        return date.strftime('%Y-%m-%dT%H:%M%:%SZ')

    def run(self):

        parameters = self.configuration.parameters
        objects = parameters[KEY_OBJECTS]

        date_from = self.get_date(parameters[KEY_DATE_FROM])

        if len(objects) == 0:
            logging.info("No objects chosen for download.")
            sys.exit(0)

        else:
            self.client.refresh_token()
            self.write_state_file({'#refresh_token': self.client.auth.new_refresh_token})

            for obj in objects:

                logging.info(f"Downloading data for object: {obj}.")

                if obj == 'billing_entries':
                    all_entries = self.client.get_billing_entries(date_from)

                    billing_entries_tdf = self.create_out_table_definition('billing_entries',
                                                                           primary_key=hdr.BILLING_ENTRIES_PK,
                                                                           columns=hdr.BILLING_ENTRIES_COLUMNS,
                                                                           incremental=parameters[KEY_INCREMENTAL])
                    billing_entries_tdf.raw_columns = hdr.BILLING_ENTRIES_RAW_COLUMNS
                    billing_entries_tdf.json_columns = hdr.BILLING_ENTRIES_JSON_COLUMNS
                    Writer(billing_entries_tdf).write_rows(all_entries)

                else:
                    logging.error(f"Object {obj} is not supported.")
                    sys.exit(1)

                logging.info(f"Finished downloading data for object: {obj}.")


if __name__ == "__main__":
    try:
        comp = Component()
        # this triggers the run method by default and is controlled by the configuration.action parameter
        comp.execute_action()
    except UserException as exc:
        logging.exception(exc)
        exit(1)
    except Exception as exc:
        logging.exception(exc)
        exit(2)
