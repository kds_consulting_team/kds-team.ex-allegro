import json
import logging
import sys

from dataclasses import dataclass
from keboola.http_client import HttpClient

BASE_URL = 'https://api.allegro.pl/'
REFRESH_URL = 'https://allegro.pl/auth/oauth/token'


@dataclass
class OAuthCredentials:
    client_key: str
    client_secret: str
    redirect_uri: str
    refresh_token: str
    new_refresh_token: str = None


class AllegroClient(HttpClient):

    def __init__(self, client_key: str, client_secret: str, refresh_token: str, redirect_uri: str):

        self.auth = OAuthCredentials(client_key, client_secret, redirect_uri, refresh_token)
        super().__init__(base_url=BASE_URL, default_http_header={'Accept': None})

    def refresh_token(self):

        refresh_params = {
            'grant_type': 'refresh_token',
            'refresh_token': self.auth.refresh_token,
            'redirect_uri': self.auth.redirect_uri
        }
        refresh_auth = (self.auth.client_key, self.auth.client_secret)

        refresh_rsp = self.post_raw(REFRESH_URL, is_absolute_path=True, auth=refresh_auth, params=refresh_params)

        if refresh_rsp.ok:
            refresh_rsp_js = refresh_rsp.json()
            self.auth.new_refresh_token = refresh_rsp_js['refresh_token']
            _auth_hdr = {
                'Authorization': f"Bearer {refresh_rsp_js['access_token']}"
            }
            self.update_auth_header(_auth_hdr)

            logging.debug(f"New refresh token: {self.auth.new_refresh_token}.")

        else:
            logging.error("Token refresh was unsuccessful. Please, reauthorize the extractor.",
                          extra={'response': json.dumps(refresh_rsp.json())})
            sys.exit(1)

    def get_billing_entries(self, date_from: str = None):
        return self._get_paged_request('billing/billing-entries', params={'occurredAt.gte': date_from},
                                       result_key='billingEntries')

    def _get_paged_request(self, endpoint: str, params: dict = {}, result_key: str = None,
                           limit_size: int = 100) -> list:

        LIMIT = limit_size
        OFFSET = 0

        results = []
        results_complete = False

        while results_complete is False:

            par_page = {**params, **{'limit': LIMIT, 'offset': OFFSET}}
            rsp_page = self.get_raw(endpoint, params=par_page)

            if rsp_page.status_code == 200:

                _js = rsp_page.json()
                _res = _js[result_key]

                results += _res

                if len(_res) < LIMIT:
                    results_complete = True
                    return results

                else:
                    OFFSET += LIMIT

            else:
                logging.error(''.join([f"Could not download paginated data for endpoint {endpoint}.\n",
                                       f"Received: {rsp_page.status_code}."]),
                              extra={'response': json.dumps(rsp_page.json())})
                sys.exit(1)
